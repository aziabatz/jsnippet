package sample.snippet;

import java.io.Serializable;
import java.util.HashMap;

public class Snippet implements Serializable {
    private static Snippet instance = null;

    static final long serialVersionUID = 1L;

    private HashMap<String, Tag> tags;

    private Snippet() {
        tags = new HashMap<>();
    }

    public static Snippet snippetInstance() {
        if (instance == null)
            instance = new Snippet();
        return instance;
    }

    public static Snippet getInstance() {
        return instance;
    }

    public HashMap<String, Tag> getTags() {
        return tags;
    }

    public static void setInstance(Snippet obj) {
        instance = obj;
    }

    public void setTags(HashMap<String, Tag> tags) {
        this.tags = tags;
    }

    public void addTag(String name) {
        tags.putIfAbsent(name, new Tag(name));
    }

    public Tag removeTag(String name) {
        Tag t = tags.remove(name);
        return t;
    }
}
