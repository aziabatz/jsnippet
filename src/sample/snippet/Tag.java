package sample.snippet;

import java.io.Serializable;
import java.util.HashSet;

public class Tag implements Serializable {
    private HashSet<Entry> entrySet;
    private String name;

    public Tag(String name) {
        this.name = name;
        entrySet = new HashSet<>();
    }

    public HashSet<Entry> getEntrySet() {
        return entrySet;
    }

    public void setEntrySet(HashSet<Entry> entrySet) {
        this.entrySet = entrySet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addEntry(String name) {
        return entrySet.add(new Entry(name, "New Entry..."));
    }

    public void removeEntry(String name) {
        entrySet.removeIf(entry -> entry.getTitle().equals(name));
    }
}
