package sample.snippet;

import java.io.Serializable;

public class Entry implements Serializable {
    private String text;
    private String title;

    public Entry(String title, String text) {
        this.text = text;
        this.title = title;
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}