package sample.fx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import sample.snippet.Entry;
import sample.snippet.Snippet;
import sample.snippet.Tag;

import java.util.stream.Collectors;

public class EntryListView extends ListView<String> implements EventHandler<MouseEvent>, Refreshable {
    private Entry selectedEntry;
    private Tag selectedTag;
    private Snippet snippet;
    private TextArea textArea;

    public EntryListView(Snippet snippet, TextArea textArea) {
        this.snippet = snippet;
        this.textArea = textArea;
        setOnMouseClicked(this::handle);
        //refreshView();
    }

    @Override
    public void handle(MouseEvent event) {
        String entryName = getSelectionModel().getSelectedItem();
        Entry e = null;
        if (selectedTag != null && !selectedTag.getEntrySet().isEmpty()) {
            for (Entry entry :
                    selectedTag.getEntrySet()) {
                if (entry.getTitle() == getSelectionModel().getSelectedItem()) {
                    e = entry;
                    break;
                }
            }
        }

        if (e != null)
            textArea.setText(e.getText());

        selectedEntry = e;
    }

    @Override
    public void refreshView() {
        ObservableList<String> objectObservableSet = FXCollections.observableArrayList(selectedTag.
                getEntrySet().
                stream().
                map(Entry::getTitle).
                collect(Collectors.toList()));
        setItems(objectObservableSet);
    }

    public Entry getSelectedEntry() {
        return selectedEntry;
    }

    public void setSelectedEntry(Entry selectedEntry) {
        this.selectedEntry = selectedEntry;
    }

    public Tag getSelectedTag() {
        return selectedTag;
    }

    public void setSelectedTag(Tag selectedTag) {
        this.selectedTag = selectedTag;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }
}
