package sample.fx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.MenuBar;
import sample.snippet.Snippet;
import sample.snippet.Tag;

import java.util.Optional;

public class SnippetScene {
    private MenuBar menuBar;
    private TagListView tagList;
    private EntryListView entryList;
    private TextArea textArea;
    private Snippet snippet;
    private ObservableList<String> dynamicOList;

    public SnippetScene(Stage stage, Snippet s) {
        snippet = s;
        StackPane root = new StackPane();
        stage.setTitle("JSnippet");
        menuBar = new MenuBar();
        textArea = new TextArea();
        entryList = new EntryListView(snippet, textArea);
        tagList = new TagListView(snippet, entryList);

        dynamicOList = FXCollections.observableArrayList(Snippet.getInstance().getTags().keySet());
        tagList.setItems(dynamicOList);

        VBox fColumn = new VBox(5);
        VBox sColumn = new VBox(5);
        TextField tagSearch = new TextField("tags...");
        TextField entrySearch = new TextField("entries...");


        Image search = new Image(getClass().getResourceAsStream("/search.png"));
        Button tagButton = new Button("Search");
        //tagButton.setGraphic(new ImageView(search));

        Button entryButton = new Button("Search");
        //entryButton.setGraphic(new ImageView(search));

        Button addTagButton = new Button("+");
        Button removeTagButton = new Button("-");
        Button addEntryButton = new Button("+");
        Button removeEntryButton = new Button("-");

        addTagButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                TextInputDialog dialog = new TextInputDialog("Tag");

                dialog.setTitle("Tag Name");
                dialog.setHeaderText("Enter name:");
                //dialog.setContentText("Name:");

                Optional<String> result = dialog.showAndWait();

                result.ifPresent(name -> {
                    snippet.addTag(name);
                    tagList.refreshView();
                });
            }
        });

        addEntryButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TextInputDialog dialog = new TextInputDialog("Entry");
                dialog.setTitle("Entry Name");
                dialog.setHeaderText("Enter name:");
                Optional<String> result = dialog.showAndWait();
                result.ifPresent(name -> {
                    tagList.getSelectedTag().addEntry(name);
                    entryList.refreshView();
                });
            }
        });

        removeTagButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Tag tag = tagList.getSelectedTag();
                snippet.removeTag(tag.getName());
                tagList.refreshView();
            }
        });

        removeEntryButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Tag tag = tagList.getSelectedTag();
                tag.removeEntry(entryList.getSelectedEntry().getTitle());
                entryList.refreshView();
            }
        });

        fColumn.getChildren().addAll(new HBox(tagSearch, tagButton, addTagButton, removeTagButton), tagList);
        sColumn.getChildren().addAll(new HBox(entrySearch, entryButton, addEntryButton, removeEntryButton), entryList);


        HBox content = new HBox(5);
        content.getChildren().addAll(fColumn, sColumn, textArea);

        menuBar.useSystemMenuBarProperty().set(true);
        root.getChildren().addAll(menuBar, content);

        textArea.setOnInputMethodTextChanged(new EventHandler<InputMethodEvent>() {
            @Override
            public void handle(InputMethodEvent event) {
                getEntryList().getSelectedEntry().setText(textArea.getText());
            }
        });

        textArea.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    getEntryList().getSelectedEntry().setText(textArea.getText());
                }
            }
        });


        stage.setScene(new Scene(root));
        stage.setMinHeight(stage.getHeight());
        stage.setMinWidth(stage.getWidth());
        stage.setResizable(true);
        stage.show();
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }

    public void setMenuBar(MenuBar menuBar) {
        this.menuBar = menuBar;
    }

    public TagListView getTagList() {
        return tagList;
    }

    public void setTagList(TagListView tagList) {
        this.tagList = tagList;
    }

    public EntryListView getEntryList() {
        return entryList;
    }

    public void setEntryList(EntryListView entryList) {
        this.entryList = entryList;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }
}
