package sample.fx;

public interface Refreshable {
    void refreshView();
}
