package sample.fx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import sample.snippet.Entry;
import sample.snippet.Snippet;
import sample.snippet.Tag;

import java.util.LinkedList;
import java.util.List;


public class TagListView extends ListView<String> implements EventHandler<MouseEvent>, Refreshable {
    private Tag selectedTag;
    private Snippet snippet;
    private EntryListView entryList;

    public TagListView(Snippet snippet, EntryListView entryList) {
        this.snippet = snippet;
        this.entryList = entryList;
        setOnMouseClicked(this::handle);
        refreshView();
    }

    @Override
    public void handle(MouseEvent event) {
        List<String> l = new LinkedList<>();
        Tag t = snippet.getTags().get(getSelectionModel().getSelectedItem());
        if (t != null) {
            entryList.setSelectedTag(t);
            selectedTag = t;
            if (!t.getEntrySet().isEmpty()) {
                for (Entry e :
                        t.getEntrySet()) {
                    l.add(e.getTitle());
                }
            }
        }
        entryList.setItems(FXCollections.observableList(l));
    }

    public void refreshView() {
        ObservableList<String> objectObservableSet = FXCollections.observableArrayList(snippet.getTags().keySet());
        setItems(objectObservableSet);
    }

    public Tag getSelectedTag() {
        return selectedTag;
    }

    public void setSelectedTag(Tag selectedTag) {
        this.selectedTag = selectedTag;
    }

    public EntryListView getEntryList() {
        return entryList;
    }

    public void setEntryList(EntryListView entryList) {
        this.entryList = entryList;
    }
}
