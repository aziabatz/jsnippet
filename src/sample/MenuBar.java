package sample;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

public class MenuBar extends javafx.scene.control.MenuBar {
    private Menu file;
    private Menu edit;
    private Menu view;
    private Menu help;

    public MenuBar() {
        file = new Menu("File");
        edit = new Menu("Edit");
        view = new Menu("View");
        help = new Menu("Help");
        createFileMenu();
        this.getMenus().addAll(file, edit, view, help);
    }

    private void createFileMenu() {
        file.getItems().add(new MenuItem("Open"));
        file.getItems().add(new MenuItem("Save"));
        file.getItems().add(new MenuItem("Save As"));
        file.getItems().add(new MenuItem("-------"));
        file.getItems().add(new MenuItem("Exit"));
    }

    private void createEditMenu() {

    }

    private void createViewMenu() {

    }

    private void createHelpMenu() {

    }
}
