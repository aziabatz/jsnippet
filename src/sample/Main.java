package sample;

import javafx.application.Application;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import sample.fx.SnippetScene;
import sample.snippet.Snippet;

import java.io.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        StackPane root = new StackPane();
        primaryStage.setTitle("JSnippet");
        new SnippetScene(primaryStage, Snippet.snippetInstance());
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        if (new File("object.txt").exists()) {
            FileInputStream fileInputStream = new FileInputStream("object.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Snippet.setInstance((Snippet) objectInputStream.readObject());
            objectInputStream.close();
            fileInputStream.close();
        } else {
            Snippet s = Snippet.snippetInstance();
        }

        // Snippet s = Snippet.snippetInstance();
        launch(args);

        FileOutputStream fileOutputStream = new FileOutputStream("object.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(Snippet.getInstance());
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
